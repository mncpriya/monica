import "@blueprintjs/core/lib/css/blueprint.css"
import { Button } from "@blueprintjs/core"
import React from 'react';
//import { Button, FormGroup, FormControl, ControlLabel } from "react-bootstrap";
import "./App.css";


class SignupForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            password:'',
            error: false
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChangePassword = this.handleChangePassword.bind(this);
    };


    handleChangePassword(event) {
        this.setState({password: event.target.value, error: false});
    }

    handleChange(event) {
        this.setState({name: event.target.value,  error: false});
    }

    handleSubmit(event) {
        event.preventDefault();
        if(!this.state.name || !this.state.password) {
            this.setState({error: true})
        } else {
           console.log("The name was submitted as: " + this.state.name);
        }
     }

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <label class="bp3-file-input .modifier">
                    <input type="file"/>
                    Username:
                  <br/>  <input type="text" value={this.state.name} onChange={this.handleChange} placeholder={"Username"} /> <br/><br />
                    Password:
                    <br/><input type="password" value={this.state.password} onChange={this.handleChangePassword} placeholder={"Password"} /><br />
                </label>
                {this.state.error && <span style={{color: "red"}}>Please enter the above fields</span>}

                <Button intent="success" text="Submit"  />
            </form>
        );
    }
}

export default SignupForm;
