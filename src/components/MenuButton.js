import React, { Component } from 'react';
import './MenuButton.css';

class MenuButton extends Component{
  render(){
    return(

      <button id="roundButton" onMouseDown={this.props.handleMouseDown} onMouseUp={this.props.handleMouseUp}>  </button>

    );
  }
}
export default MenuButton;