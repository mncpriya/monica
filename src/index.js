import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import MenuContainer from './App';


ReactDOM.render(<MenuContainer/>, document.getElementById('root'));

