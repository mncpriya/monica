import React, { Component } from "react";
import "./Menu.css";
import "./MenuRight";
class Menu extends Component {
  render() {
    let visibility = "hide";

    if (this.props.menuVisibility) {
      visibility = "show";
    }
    const leftDrawer = (
      <div className={'backdrop'} onClick={this.props.close}>
        <div className={`flyoutMenuLeft ${visibility} animate-left`}>
          <h2><a href="#">Select</a><a href="#"> On</a></h2>
          <h2><a href="#">insert</a><a href="#"> Off</a></h2>
          <h2><a href="#">update</a></h2>
          <h2><a href="#">delete</a></h2>
        </div>
      </div>
    );

    const rightDrawer = (
      <div className={'backdrop'} onClick={this.props.close}>
        <div className={`flyoutMenuRight ${visibility} animate-right`}>
          <h2><a href="#">Select</a><a href="#"> On</a></h2>
          <h2><a href="#">insert</a><a href="#"> Off</a></h2>
          <h2><a href="#">update</a></h2>
          <h2><a href="#">delete</a></h2>
        </div>
      </div>
    );

    const TopDrawer = (
      <div className={'backdrop'} onClick={this.props.close}>
        <div className={`flyoutMenuTop ${visibility} animate-top`}>
          <h2><a href="#">Select</a><a href="#"> On</a></h2>
          <h2><a href="#">insert</a><a href="#"> Off</a></h2>
          <h2><a href="#">update</a></h2>
          <h2><a href="#">delete</a></h2>
        </div>
      </div>
    );

    const BottomDrawer = (
      <div className={'backdrop'} onClick={this.props.close}>
        <div className={`flyoutMenuBottom ${visibility} animate-bottom`}>
          <h2><a href="#">Select</a><a href="#"> On</a></h2>
          <h2><a href="#">insert</a><a href="#"> Off</a></h2>
          <h2><a href="#">update</a></h2>
          <h2><a href="#">delete</a></h2>
        </div>
      </div>
    );


      let content = null;
      switch(this.props.openFrom) {
        case 'left': {
          content = leftDrawer;
          break;
        }
        case 'right': {
          content = rightDrawer;
          break;
        }
        case 'top': {
          content = TopDrawer;
          break;
        }
        case 'bottom': {
          content = BottomDrawer;
          break;
        }
      }

    return (
      this.props.menuVisibility ? content : null
    )
  }
}

export default Menu;