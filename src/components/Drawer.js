import React from 'react';

class Drawer extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      top: null,
      bottom: null,
      left: null,
      right: null
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  render(){
    return <span className="menu navigation-menu">Menu</span>
  }
  return(){
    let className = 'menu';
    if(this.props.isActive){
      className += ' menu-active';
    }
    return <span className={className}></span>
  }
}

export default Drawer;

