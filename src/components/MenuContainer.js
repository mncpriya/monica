import React, { Component } from "react";
import MenuButtons from './MenuButton';
import Menus from './menu';

class MenuContainer extends Component{

  state={
    visible: false,
    left: false,
    right: false,
    top: false,
    bottom: false
  };

  _reset = () => { this.setState({
   visible: false,
   left: false,
   right: false,
   top: false,
   bottom: false})
 };

  toggleDrawerRight = () => {
    this._reset();

    this.setState({
      visible: true,
      right: true,
    });
  };

  toggleDrawerLeft = () => {
    this._reset();

    this.setState({
      visible: true,
      left: true,
    });
  };

  toggleDrawerTop = () => {
    this._reset();

    this.setState({
      visible: true,
      top: true,
    });
  };

  toggleDrawerBottom = () => {
    this._reset();

    this.setState({
      visible: true,
      bottom: true,
    });
  };

  close = () => {
    this.setState({
      visible: !this.state.visible
    });
  };

  render(){
    return(
      <React.Fragment>

        <button onClick={this.toggleDrawerLeft}>Open Left</button>
        {
          this.state.left?
            <Menus menuVisibility={this.state.visible} close={this.close} openFrom={'left'}/>
            : null
        }


        <button onClick={this.toggleDrawerRight}>Open Right</button>
        {
          this.state.right?
            <Menus menuVisibility={this.state.visible} close={this.close} openFrom={'right'}/>
            : null
        }

        <button onClick={this.toggleDrawerTop}>Open Top</button>
        {
          this.state.top?
            <Menus menuVisibility={this.state.visible} close={this.close} openFrom={'top'}/>
            : null
        }

        <button onClick={this.toggleDrawerBottom}>Open Bottom</button>
        {
          this.state.bottom?
            <Menus menuVisibility={this.state.visible} close={this.close} openFrom={'bottom'}/>
            : null
        }

        {/*<button onClick={this.toggleDrawer}>Open Top</button>
        <button onClick={this.toggleDrawer}>Open Bottom</button>
        */}
{/*        <div tabIndex={0}
        role="button"
        onClick={this.toggleDrawerLeft}></div>*/}
        {/*<div>
          <p> Can U Spot The Options BELOW</p>
          <ul>
            <li>Select</li>
            <li>Insert</li>
            <li>Modify</li>
            <li>Delete</li>
            <li>Undo</li>
            <li>Redo</li>
          </ul>
        </div>

        <button onClick={this.openDrawer}>OnClick</button>
        <Menus menuVisibility={this.state.visible} close={this.close}/>
        <div>
          <p> Can U Spot The Options BELOW</p>
          <ul>
            <li>Open</li>
            <li>Edit</li>
            <li>Modify</li>
            <li>Rename</li>
            <li>Undo</li>
            <li>Redo</li>
          </ul>
        </div>*/}

      </React.Fragment>

    );
  }
}


export default MenuContainer;