import React, { Component } from "react";
import "./Menu.css";

class MenuRight extends Component {
  render() {
    let visibility = "hide";
    if (this.props.menuVisibility) {
      visibility = "show";
    }
    return (

      this.props.menuVisibility?
        <div className={'backdrop'} onClick={this.props.close}>
          <div id="flyinMenu"
               onMouseDown={this.props.handleMouseDown} onMouseUp={this.props.temporaryDrawer}
               className={visibility}>
            <h2><a href="#">LinkIN</a><a href="#"> On</a></h2>
            <h2><a href="#">Rename</a><a href="#"> Off</a></h2>
            <h2><a href="#">update</a></h2>
            <h2><a href="#">delete</a></h2>

          </div>
        </div>:
        null

    );

  }
}

export default MenuRight;