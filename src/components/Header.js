import React, {Component} from 'react';
import {Button, FormGroup, InputGroup,ButtonGroup,Icon,Intent,Tooltip,Position} from '@blueprintjs/core';
import {INTENT_WARNING, MINIMAL} from "@blueprintjs/core/lib/esnext/common/classes";
import {IExampleProps,Example} from '@blueprintjs/core';


class App extends Component {
  render() {


    return (
      <div>
        <span className="bp3-tag">Form to Sign-IN </span><br/><br/>
        <form onSubmit={this.submit}></form>
        <FormGroup
        labelInfo={true}>

          <label className="bp3-input-file" >
            <span className="bp3-email-ipload-input" >UserName</span><br/><br/>

             <InputGroup
             disabled={false}
             large={true}
             placeholder="Enter the username..."

             />

       <span className="bp3-password-ipload-input">Password</span> <br/><br/>

            <InputGroup
              disabled={true}
              large={true}
              placeholder="Enter the password..."
            />

        <Button type="submit">Submit The Form</Button>

          </label>
        </FormGroup>
      </div>
    )
  }
}
export default App;