import React, { Component } from 'react';
import './App.css';
import MenuContainer from './components/MenuContainer';
import "@blueprintjs/core/lib/css/blueprint.css"

class App extends Component {
  render() {
    return (
      <div className="App">
        <MenuContainer />
      </div>
    );
  }
}

export default App;
